import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, Button, Image } from 'react-native'
import Actionsheet from 'react-native-enhanced-actionsheet'
import AsyncStorage from '@react-native-async-storage/async-storage';

let COUNT = 0
var tom = require('./tom.png')
var harry = require('./harry.jpg')

const OPTIONS = [
  {id: COUNT++, label: 'Tom Jerry', image : tom}, 
  {id: COUNT++, label: 'Harry Potter', image : harry}, 
]

export default class App extends Component {

  constructor(props) {
    super(props)
    
    this.state = {
      visible: false,
      selected: 0
    }
  }

  writeData = async () => {
      try {
          const selectedOption = OPTIONS.find((e) => e.id === this.state.selected)
          const jsonValue = JSON.stringify(selectedOption)
        await AsyncStorage.setItem('@story', jsonValue)
      } catch (e) {
        // saving error
      console.log('====>Error');
      }
    }

    readData = async () => {
        try {
          const jsonValue = await AsyncStorage.getItem('@story')
      //     return jsonValue != null ? JSON.parse(jsonValue) : null;
          console.log(jsonValue);
        } catch(e) {
          // error reading value
        }
      }
      

  componentDidMount(){
    this.writeData()
    this.readData()
  }


  render() {
    const selectedOption = OPTIONS.find((e) => e.id === this.state.selected)
    return (
      <View style={styles.container}>
      <View style={{marginTop: 40}}>
          <Text>{selectedOption.label}</Text>
          <Image 
            source = {selectedOption.image}
            style = {{width:200, height: 100}}
          />
        </View>
        <Button
          title="Choose Story"
          color="#841584"
          onPress={() => this.setState({visible: true})}
        />
        <Actionsheet 
          visible={this.state.visible}
          data={OPTIONS} 
          title={'Select an option'}
          selected={this.state.selected}
          onOptionPress={(e) => this.setState({visible: false, selected: e.id})}
          onCancelPress={() => this.setState({visible: false})}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    flex : 1,
    alignItems : "center",
    justifyContent : "center"
  }
})


